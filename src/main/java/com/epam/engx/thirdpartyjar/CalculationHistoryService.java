package com.epam.engx.thirdpartyjar;

public interface CalculationHistoryService {

    History retrieveHistory(Service service);

}
